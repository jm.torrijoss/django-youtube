
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.CharField(max_length=12, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=256)),
                ('link', models.CharField(max_length=256)),
                ('selected', models.BooleanField(default=False)),
            ],
        ),
    ]
